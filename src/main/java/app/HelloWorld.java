package app;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println(String.format("Hello %s!",
                ArrayUtils.isNotEmpty(args) && StringUtils.isNotBlank(args[0]) ? args[0] : "World"));
    }
}
