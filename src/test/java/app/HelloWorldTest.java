package app;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class HelloWorldTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void testOutput() {
        HelloWorld.main(new String[]{"Martin"});
        Assert.assertEquals("Hello Martin!\n", systemOutRule.getLog());
    }

    @Test
    public void testOutputNoArgs() {
        HelloWorld.main(null);
        Assert.assertEquals("Hello World!\n", systemOutRule.getLog());
    }
}
